# Privacy Policies

This is a public repo to host privacy policies required by Meta. Privacy policies are generated at https://app.privacypolicies.com/.
